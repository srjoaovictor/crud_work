<?php
$sql = "SELECT * FROM tb_usuarios";
$res = $conn->query($sql);
$qtd = $res->num_rows;
if ($qtd > 0) {
    print "<table class='table'>";
        print "<tr>";
        print "<th>ID</th>";
        print "<th>Matricula</th>";
        print "<th>Funcionario</th>";
        print "<th>Unidade Operacional</th>";
        print "<th>Polo/Base</th>";
        print "<th>Gerente</th>";
        print "<th>Imediato</th>";
        print "<th>Acoes</th>";
        print "</tr>";
    while ($row = $res->fetch_object()) {
        print "<tr>";
        print "<td>".$row->id_user."</td>";
        print "<td>".$row->mat_user."</td>";
        print "<td>".$row->nome_user."</td>";
        print "<td>".$row->unidade_user."</td>";
        print "<td>".$row->base_user."</td>";
        print "<td>".$row->gerente_user."</td>";
        print "<td>".$row->imediato_user."</td>";
        print "<td>
            <button onclick=\"location.href='?page=edit&id_user=".$row->id_user."';\" class='btn btn-success btn-sm'>Editar</button>
            <button onclick=\"if(confirm('Tem certeza que deseja excluir?')){location.href='?page=reg&acao=excluir&id_user=".$row->id_user."';}else{false;}\" class='btn btn-danger btn-sm'>Excluir</button>
        </td>";
        print "</tr>";
    }
    print "</table>";
}else {
    print "<p class='alert alert-danger'>Nao encontrou resultados!</p>";
}
?>
    