<form action="?page=reg" method="POST">
    <input type="hidden" name="acao" value="cadastrar">
    <div>
        <label>Matricula</label>
        <input type="text" name="mat_user" class="form-control">
    </div>
    <div>
        <label>Funcionario</label>
        <input type="text" name="nome_user" class="form-control">
    </div>
    <div>
        <label>Unidade Operacional</label>
        <input type="text" name="unidade_user" class="form-control">
    </div>
    <div>
        <label>Polo/Base</label>
        <input type="text" name="base_user" class="form-control">
    </div>
    <div>
        <label>Gerente</label>
        <input type="text" name="gerente_user" class="form-control">
    </div>
    <div>
        <label>Imediato</label>
        <input type="text" name="imediato_user" class="form-control">
    </div>
    <div class="mt-3">
        <button type="submit" class="btn btn-secondary">Cadastrar</button>
    </div>
</form>