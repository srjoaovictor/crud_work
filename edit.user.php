<?php
$sql = "SELECT * FROM tb_usuarios WHERE id_user=".$_REQUEST["id_user"];
$res = $conn->query($sql);
$row = $res->fetch_object();
?>
<form action="?page=reg" method="POST">
    <input type="hidden" name="acao" value="editar">
    <input type="hidden" name="id_user" value="<?php print $row->id_user; ?>">
    <div>
        <label>Matricula</label>
        <input type="text" name="mat_user" value="<?php print $row->mat_user; ?>" class="form-control">
    </div>
    <div>
        <label>Funcionario</label>
        <input type="text" name="nome_user" value="<?php print $row->nome_user; ?>" class="form-control">
    </div>
    <div>
        <label>Unidade Operacional</label>
        <input type="text" name="unidade_user" value="<?php print $row->unidade_user; ?>" class="form-control">
    </div>
    <div>
        <label>Polo/Base</label>
        <input type="text" name="base_user" value="<?php print $row->base_user; ?>" class="form-control">
    </div>
    <div>
        <label>Gerente</label>
        <input type="text" name="gerente_user" value="<?php print $row->gerente_user; ?>" class="form-control">
    </div>
    <div>
        <label>Imediato</label>
        <input type="text" name="imediato_user" value="<?php print $row->imediato_user; ?>" class="form-control">
    </div>
    <div class="mt-3">
        <button type="submit" class="btn btn-secondary">Atualizar</button>
    </div>
</form>