<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Funcionarios</title>
        <link href="css/bootstrap.min.css" rel="stylesheet">
    </head>
    <body>
        <nav class="navbar bg-light">
            <form class="container-fluid justify-content-center">
                <div class="btn-toolbar" role="toolbar" aria-label="Toolbar with button groups">
                    <div class="btn-group me-2" role="group" aria-label="First button">
                        <a class="btn btn-sm btn-secondary" href="?page=view" role="button">Visualizar</a>
                    </div>
                    <div class="btn-group me-2" role="group" aria-label="Second button">
                        <a class="btn btn-sm btn-secondary" href="?page=cad" role="button">Cadastrar</a>
                    </div>
                    <div class="btn-group me-2" role="group" aria-label="Third button">
                        <a class="btn btn-sm btn-outline-danger" href="index.php" role="button">Retornar</a>                
                    </div>
                </div>                
            </form>
        </nav>
        <div class="container">
            <div class="row">
                <div class="col mt-5">
                    <?php
                    include("config.php");        
                    switch (@$_REQUEST["page"]) {
                        case 'view':
                            include("view.user.php");
                        break;

                        case 'cad':
                            include("cad.user.php");
                        break;

                        case 'reg':
                            include("reg.user.php");
                        break;

                        case 'edit':
                            include("edit.user.php");
                        break;
            
                    default:
                    print "Area de Funcionarios";
                    }
                    ?>
                </div>
            </div>
        </div>        
    <script src="js/bootstrap.bundle.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.6/dist/umd/popper.min.js" integrity="sha384-oBqDVmMz9ATKxIep9tiCxS/Z9fNfEXiDAYTujMAeBAsjFuCZSmKbSSUnQlmh/jp3" crossorigin="anonymous"></script>
    <script src="js/bootstrap.min.js"></script>
    </body>
</html>