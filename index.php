<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Sistemas EPI Checkout</title>
        <link href="css/bootstrap.min.css" rel="stylesheet">
    </head>
    <body>
        <nav class="navbar bg-light">
            <form class="container-fluid justify-content-center">
                <div class="btn-toolbar" role="toolbar" aria-label="Toolbar with button groups">
                    <div class="btn-group me-2" role="group" aria-label="First button">
                        <a class="btn btn-sm btn-secondary disabled" href="#" role="button" aria-disabled="true">Vistoria</a>
                    </div>
                    <div class="btn-group me-2" role="group" aria-label="Second button">
                        <a class="btn btn-sm btn-secondary" href="user.php" role="button">Funcionarios</a>
                    </div>
                    <div class="btn-group me-2" role="group" aria-label="Third button">
                        <a class="btn btn-sm btn-secondary disabled" href="#" role="button" aria-disabled="true">Equipamentos</a>                
                    </div>
                </div>                
            </form>
        </nav>        
    <script src="js/bootstrap.bundle.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.6/dist/umd/popper.min.js" integrity="sha384-oBqDVmMz9ATKxIep9tiCxS/Z9fNfEXiDAYTujMAeBAsjFuCZSmKbSSUnQlmh/jp3" crossorigin="anonymous"></script>
    <script src="js/bootstrap.min.js"></script>
    </body>
</html>